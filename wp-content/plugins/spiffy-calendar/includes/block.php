<?php
/*
 ** Spiffy Calendar Gutenberg Block
 **
 ** This code is included during the "init" action.
 **
 ** Copyright Spiffy Plugins
 **
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if (!class_exists("SPIFFYCALBlock")) {
class SPIFFYCALBlock {

	/*
	** Construct the block
	*/
	function __construct () {
		global $wpdb, $spiffy_calendar;
		
		// Get calendar category list
		$sql = "SELECT * FROM " . WP_SPIFFYCAL_CATEGORIES_TABLE;
		if ($spiffy_calendar->current_options['alphabetic_categories'] == 'true') $sql .= " ORDER BY category_name";
		$cats = $wpdb->get_results($sql);
		$cats_array = array();
		foreach($cats as $cat) {
			$cats_array[] = array( 'value' => $cat->category_id,
									'label' => esc_html(stripslashes($cat->category_name))
								);
			}
			
		// Register our script and associated data
		wp_register_script(
			'spiffy-calendar-block',
			plugins_url( '/js/block.js', __DIR__ ),
			array( 'wp-blocks', 'wp-element', 'wp-components', 'wp-editor', 'wp-i18n' ),
			filemtime( plugin_dir_path(__DIR__) . 'js/block.js'));
		wp_add_inline_script( 
			'spiffy-calendar-block', 
'/* <![CDATA[ */
' . 'var spiffycal_bonus="'.is_plugin_active( 'spiffy-calendar-addons/spiffy-calendar-addons.php').'";
'. 'var spiffycal_cats='. json_encode($cats_array, JSON_PRETTY_PRINT) . ';
' . '/* ]]> */',
			'before' );
		
		// Register our styles so the calendar displays in backend blocks
		wp_register_style(
			'spiffycal-styles',
			plugins_url( 'styles/default.css', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'styles/default.css' )
		);

	register_block_type( 'spiffy-calendar/main-block', array(
			'attributes' => array(
								'expand' => array(
										'type' => 'boolean',
										'default' => true,
									),
								'display' => array(
										'type' => 'select',
										'default' => 'spiffy-calendar',
									),
								'title' => array(
										'type' => 'string',
									),
								'cat_list' => array (
										'type' => 'select'
									),
								'limit' => array (
										'type' => 'range',
										'default' => 0
									),
								'style' => array (
										'type' => 'select'
									),
								'num_columns' => array (
										'type' => 'range',
										'default' => 3
									),									
								'none_found' => array(
										'type' => 'string',
									),
								'show_date' => array(
										'type' => 'checkbox',
										'default' > 'false',
									),
								'manage' => array(
										'type' => 'checkbox',
										'default' > 'false',
									),
								'manage_title' => array(
										'type' => 'string',
										'default' => __('Your events', 'spiffy-calendar'),
									),
								),
			'editor_script' => 'spiffy-calendar-block',
			'style' => 'spiffycal-styles',
			'render_callback' => array($this, 'block_render'),
		) );	
	}

	/**
	 * Render the block.
	 *
	 * @param array $attributes The attributes that were set on the block.
	 */
	public function block_render( $attributes ) {
		// Pull out the values that are not shortcode attributes
		$display = $attributes['display'];
		$expand =  $attributes['expand'];
		unset ($attributes['display']);
		unset ($attributes['expand']);
		
		// Encode the shortcode attributes
		$shortcode_atts = '';
		foreach ($attributes as $key => $value) {
			if ($value != '') {
				$shortcode_atts .= ' ' . $key . '="' . ((is_array($value))? implode(',', $value) : $value) . '"';
			}
		}
		
		// Render the output appropriately
		if ($expand) return do_shortcode('[' . $display . $shortcode_atts . ']');
		return '[' . $display . $shortcode_atts . ']';
	}

} // end of class
}

if (class_exists("SPIFFYCALBlock")) {
	$spiffy_calendar_block = new SPIFFYCALBlock();
}

?>